import datasets
import evaluate
import transformers

task_type = "text-classification"

model_id = "juliensimon/distilbert-amazon-shoe-reviews"

dataset_id = "juliensimon/amazon-shoe-reviews"
label_column = "labels"
label_mapping = {
    "LABEL_0": 0,
    "LABEL_1": 1,
    "LABEL_2": 2,
    "LABEL_3": 3,
    "LABEL_4": 4,
}

data = datasets.load_dataset(dataset_id, split="test")
print(data)
metric = evaluate.load("accuracy")
evaluator = evaluate.evaluator(task_type)


def evaluate_pipeline(pipeline):
    results = evaluator.compute(
        model_or_pipeline=pipeline,
        data=data,
        metric=metric,
        label_column=label_column,
        label_mapping=label_mapping,
    )
    return results


print("*** Original model")
classifier = transformers.pipeline(task_type, model_id)
results = evaluate_pipeline(classifier)
print(results)

print("*** ONNX")

from optimum.onnxruntime import ORTModelForSequenceClassification
from optimum.pipelines import pipeline

model = ORTModelForSequenceClassification.from_pretrained(
    model_id, from_transformers=True
)
tokenizer = transformers.AutoTokenizer.from_pretrained(model_id)
model.save_pretrained("./model_onnx")
tokenizer.save_pretrained("./model_onnx")
classifier_onnx = pipeline(task_type, model=model, tokenizer=tokenizer)
results = evaluate_pipeline(classifier_onnx)
print(results)

print("*** ONNX optimizer")

from optimum.onnxruntime import ORTOptimizer
from optimum.onnxruntime.configuration import OptimizationConfig

optimizer = ORTOptimizer.from_pretrained(model)
optimizer.optimize(
    OptimizationConfig(
        optimization_level=99, # 1, 2 or 99
    ),
    save_dir="./model_onnx",
)
model_optimized = ORTModelForSequenceClassification.from_pretrained(
    "./model_onnx", file_name="model_optimized.onnx"
)
classifier_optimized = pipeline(task_type, model=model_optimized, tokenizer=tokenizer)
results = evaluate_pipeline(classifier_optimized)
print(results)

print("*** ONNX quantizer")

from optimum.onnxruntime import ORTQuantizer
from optimum.onnxruntime.configuration import AutoQuantizationConfig

quantizer = ORTQuantizer.from_pretrained(model)
qconfig = AutoQuantizationConfig.avx512_vnni(is_static=False, per_channel=True)
quantizer.quantize(save_dir="./model_onnx", quantization_config=qconfig)
model_quantized = ORTModelForSequenceClassification.from_pretrained(
    "./model_onnx", file_name="model_quantized.onnx"
)
classifier_quantized = pipeline(task_type, model=model_quantized, tokenizer=tokenizer)
results = evaluate_pipeline(classifier_quantized)
print(results)
