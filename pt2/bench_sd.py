import torch
from diffusers import DiffusionPipeline
import intel_extension_for_pytorch as ipex
import time

device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")

pipe = DiffusionPipeline.from_pretrained("runwayml/stable-diffusion-v1-5").to(device)
prompt = "a viking charging into battle"

def bench(n=10, steps=25, batch_size=1):
    # Warmup
    images = pipe(prompt, num_inference_steps=steps, num_images_per_prompt=batch_size).images[0]
    start = time.time()
    for _ in range(n):
        images = pipe(prompt, num_inference_steps=steps, num_images_per_prompt=batch_size).images[0]
    end = time.time()
    return (end - start) * 1000 / n

#print(f"Average time: {bench():.2f} ms")
pipe.unet = torch.compile(pipe.unet, backend="inductor")
print(f"Average time: {bench():.2f} ms")





