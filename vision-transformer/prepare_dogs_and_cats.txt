kaggle competitions download -c dogs-vs-cats

unzip dogs-vs-cats.zip train.zip
unzip train.zip

cd train
mkdir dog cat
find . -name 'dog.*' -exec mv {} dog \;
find . -name 'cat.*' -exec mv {} cat \;
cd ..
mkdir -p train2500/dog train2500/cat
ls train/dog | sort -R | tail -1250 | while read file; do cp train/dog/$file train2500/dog; done
ls train/cat | sort -R | tail -1250 | while read file; do cp train/cat/$file train2500/cat; done

aws s3 mb s3://julsimon-huggingface-dogscats
aws s3 sync train2500 s3://julsimon-huggingface-dogscats/train2500